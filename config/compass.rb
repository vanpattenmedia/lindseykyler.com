# Require some gems
require 'rgbapng'
require 'sass-globbing'
require 'susy'
require 'breakpoint'
require 'compass-normalize'

# Path to theme from project root
project_path      = "./"

# Where's stuff being spit out?
css_dir           = "public/assets/stylesheets"
images_dir        = "public/assets/images"
javascripts_dir   = "public/assets/javascripts"
fonts_dir         = "public/assets/fonts"

# Where are we pulling from?
sass_dir          = "app/assets/sass"
add_import_path   = "vendor/sass"

# Are we in development or production?
environment       = "production"

# What should environments look like?
if environment == "production"
  output_style  = :compressed
  line_comments = false
else
  output_style  = :expanded
  line_comments = true
end

# Enable relative asset paths
relative_assets = true
