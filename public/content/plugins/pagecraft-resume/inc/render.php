<?php
/**
 * Pagecraft
 * (c) 2013 Van Patten Media Inc.
 *
 * module: Resume
 */

class PagecraftResume extends PagecraftModule
{
	function __construct($pid = null)
	{
		if ( $pid === null && get_the_ID() )
		{
			$pid = get_the_ID();
			$this->ID = $pid;
		}
		else if ( $pid === null && have_posts() )
		{
			ob_start();
			the_post();
			$pid = get_the_ID();
			$this->ID = $pid;
			ob_end_clean();
		}

		if ( is_numeric($pid) )
		{
			$this->ID = $pid;
		}
	}

	function embed()
	{
		$file_id = get_field('resume');
		$url     = wp_get_attachment_url( $file_id, $this->ID );
		?>
		<p><a href="<?php echo $url; ?>" class="pc-embed-download">Download the resume as a PDF.</a></p>
		<object type="application/pdf" data="<?php echo $url; ?>" class="pc-embed-pdf">
			<iframe src="http://docs.google.com/gview?url=<?php echo $url; ?>&embedded=true" frameborder="0"></iframe>
		</object>
		<?php
	}

}
