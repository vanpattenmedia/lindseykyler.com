<?php
/*
Plugin Name: Pagecraft Resume
Plugin URI: http://pagecraftapp.com/
Description: WordPress toolkit for building easy actor websites
Version: 1.0b
Author: Van Patten Media
Author URI: http://www.vanpattenmedia.com/
Copyright: Van Patten Media Inc.
*/

/**
 * Pagecraft
 * (c) 2013 Van Patten Media Inc.
 *
 * module: Resume
 */

function pagecraft_register_resume() {
	require_once( 'inc/fields.php' );
	require_once( 'inc/render.php' );
}

add_action('pagecraft_register_module', 'pagecraft_register_resume');
