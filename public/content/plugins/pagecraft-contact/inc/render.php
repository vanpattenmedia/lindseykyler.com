<?php
/**
 * Pagecraft
 * (c) 2013 Van Patten Media Inc.
 *
 * module: Contact
 */

class PagecraftContact extends PagecraftModule
{
	public $variable = 'hello';

	function form()
	{
		?>
			<form action="" class="pagecraft-form" id="pagecraft-contact-form" method="post">
			<div class="control-group">
				<label class="control-label" for="pagecraft-contact-form-name">Name</label>
				<div class="controls">
					<input type="text" name="pc_name" id="pagecraft-contact-form-name" class="pagecraft-form-text">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="pagecraft-contact-form-email">Email</label>
				<div class="controls">
					<input type="email" name="pc_email" id="pagecraft-contact-form-email" class="pagecraft-form-text">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="pagecraft-contact-form-message">Message</label>
				<div class="controls">
					<textarea name="pc_message" id="pagecraft-contact-form-message" class="pagecraft-form-textarea"></textarea>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
				<input type="submit" class="pagecraft-form-input">
			</div>
		</form>
		<?php
	}

}
