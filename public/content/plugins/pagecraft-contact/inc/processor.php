<?php

function email( $array )
{
	$to 		= 'chris@vanpattenmedia.com';
	$name       = strip_tags( $array['author'] );
	$from 		= strip_tags( $array['email'] );
	$message 	= strip_tags( $array['body'] );

	$headers = 'From: '. $from . "\r\n" .
    'Reply-To: '. $from . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

	$subject = 'New message from ' . $name;

	wp_mail($to, $subject, $message, $headers);
}

function pagecraft_contact_submit()
{
	define("AKISMET_API_KEY", "2b0eecedacaa");
	require_once( __DIR__ . '/../lib/akismet/akismet.class.php');

	if( isset( $_POST['pc_name'], $_POST['pc_email'], $_POST['pc_message'] ) )
	{
		$comment = array(
			'author'    => $_POST['pc_name'],
			'email'     => $_POST['pc_email'],
			'body'      => $_POST['pc_message'],
			'permalink' => 'http://'.$_SERVER['HTTP_HOST'],
		);

		$akismet = new Akismet('http://'.$_SERVER["HTTP_HOST"], AKISMET_API_KEY, $comment);

		if($akismet->errorsExist())
		{ // Returns true if any errors exist.
			if($akismet->isError('AKISMET_INVALID_KEY'))
			{
				// Do something. Log the error or email the user
				error_log('Akismet invalid key!', 0);
			}
			elseif($akismet->isError('AKISMET_RESPONSE_FAILED'))
			{
				// Do something.
				error_log('Akismet response failed', 0);
			}
			elseif($akismet->isError('AKISMET_SERVER_NOT_FOUND'))
			{
				// Do something.
				error_log('Akismet server not found!', 0);
			}
		}
		else
		{
			// No errors, check for spam.
			if ($akismet->isSpam())
			{ // Returns true if Akismet thinks the comment is spam.
				// Do something with the spam comment.
				echo 'SPAM!';
			}
			else
			{
				// Do something with the non-spam comment.
				email( $comment );
			}
		}
	}
}
add_action( 'init', 'pagecraft_contact_submit' );
