<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id'     => 'acf_photo-gallery',
		'title'  => 'Photo Gallery',
		'fields' => array (
			array (
				'key'        => 'field_5251693504e02',
				'label'      => 'Photo Gallery',
				'name'       => 'photo_gallery',
				'type'       => 'repeater',
				'sub_fields' => array (
					array (
						'key'          => 'field_5251694304e03',
						'label'        => 'Photo',
						'name'         => 'photo',
						'type'         => 'image',
						'column_width' => '',
						'save_format'  => 'id',
						'preview_size' => 'thumbnail',
						'library'      => 'all',
					),
					array (
						'key'           => 'field_5251696104e04',
						'label'         => 'Caption',
						'name'          => 'caption',
						'type'          => 'text',
						'column_width'  => '',
						'default_value' => '',
						'placeholder'   => '',
						'prepend'       => '',
						'append'        => '',
						'formatting'    => 'none',
						'maxlength'     => '',
					),
				),
				'row_min'      => 0,
				'row_limit'    => '',
				'layout'       => 'row',
				'button_label' => 'Add Photo',
			),
		),
		'location' => array (
			array (
				array (
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'photos',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position'       => 'normal',
			'layout'         => 'no_box',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				5 => 'revisions',
				6 => 'format',
				7 => 'featured_image',
				8 => 'categories',
				9 => 'tags',
				10 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}
