<?php
/**
 * Pagecraft
 * (c) 2013 Van Patten Media Inc.
 *
 * module: Photos
 */

class PagecraftPhotos extends PagecraftModule
{
	function __construct($pid = null)
	{
		if ( $pid === null && get_the_ID() )
		{
			$pid = get_the_ID();
			$this->ID = $pid;
		}
		else if ( $pid === null && have_posts() )
		{
			ob_start();
			the_post();
			$pid = get_the_ID();
			$this->ID = $pid;
			ob_end_clean();
		}

		if ( is_numeric($pid) )
		{
			$this->ID = $pid;
		}

		// Start setting up the photos
		$this->photos = array();

		$repeater = get_field( 'photo_gallery', $this->ID );

		if ( $repeater )
		{

			foreach ( $repeater as $single )
			{
				// Get the registered image sizes
				$registered_image_sizes   = get_intermediate_image_sizes();

				// Add the 'full' image size
				$registered_image_sizes[] = 'full';

				// Add empty $sources array
				$sources = array();

				// Loop through registered image sizes and add them to the $sources array
				foreach ( $registered_image_sizes as $registered_image_size )
				{
					$src = wp_get_attachment_image_src( $single['photo'], $registered_image_size );
					$sources[$registered_image_size] = $src[0];
				}

				$this->photos[] = array(
					'id'      => $single['photo'],
					'src'     => $sources,
					'caption' => $single['caption'],
				);
			}
		}
	}
}
