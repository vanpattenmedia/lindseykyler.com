<?php
/**
 *
 * Register casestudy post type
 *
 */

if ( !post_type_exists('photos') ) {
	function register_photos_type() {
		$photos_plural   = 'Photo Galleries';
		$photos_singular = 'Photo Gallery';
		
		register_post_type(
			'photos',
			array(
				'label'           => $photos_plural,
				'description'     => '',
				'public'          => true,
				'show_ui'         => true,
				'show_in_menu'    => true,
				'capability_type' => 'page',
				'hierarchical'    => false,
				'query_var'       => true,
				'has_archive'     => true,
				'rewrite' => array(
					'slug'       => 'photos',
					'with_front' => false,
				),
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'thumbnail',
					'custom-fields',
				),
				'labels' => array (
					'name'               => $photos_plural,
					'singular_name'      => $photos_singular,
					'menu_name'          => $photos_plural,
					'add_new'            => 'Add New',
					'add_new_item'       => 'Add New ' . $photos_singular,
					'edit'               => 'Edit',
					'edit_item'          => 'Edit ' . $photos_singular,
					'new_item'           => 'New ' . $photos_singular,
					'view'               => 'View ' . $photos_singular,
					'view_item'          => 'View ' . $photos_singular,
					'search_items'       => 'Search ' . $photos_plural,
					'not_found'          => 'No ' . $photos_plural . ' Found',
					'not_found_in_trash' => 'No ' . $photos_plural . ' Found in Trash',
					'parent'             => 'Parent ' . $photos_singular,
				)
			)
		);
	}
	add_action('init', 'register_photos_type');
}
