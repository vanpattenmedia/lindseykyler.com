<?php

$page = get_page_by_path('videos');
if ( $page )
{
	$page_id = $page->ID;
}


/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_videos',
		'title' => 'Videos',
		'fields' => array (
			array (
				'key' => 'field_527000ccea083',
				'label' => 'Videos',
				'name' => 'videos',
				'type' => 'repeater',
				'instructions' => 'Input links to YouTube videos.',
				'sub_fields' => array (
					array (
						'key' => 'field_527000d7ea084',
						'label' => 'Video',
						'name' => 'video',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5270062aeb418',
						'label' => 'Caption',
						'name' => 'caption',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Video',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => $page_id,
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
