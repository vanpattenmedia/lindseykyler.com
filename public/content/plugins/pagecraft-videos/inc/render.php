<?php
/**
 * Pagecraft
 * (c) 2013 Van Patten Media Inc.
 *
 * module: Photos
 */

class PagecraftVideos extends PagecraftModule
{
	private function get_youtube_image($url)
	{
		$image_url = parse_url($url);
		if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com')
		{
			$url = explode('&',$image_url['query']);
			$url = substr($url[0],2);

			return 'http://img.youtube.com/vi/'.$url.'/1.jpg';
		}
		else
		{
			return 'http://img.youtube.com/vi/'.$url.'/5.jpg';
		}

	}

	function __construct($pid = null)
	{
		if ( $pid === null && get_the_ID() )
		{
			$pid = get_the_ID();
			$this->ID = $pid;
		}
		else if ( $pid === null && have_posts() )
		{
			ob_start();
			the_post();
			$pid = get_the_ID();
			$this->ID = $pid;
			ob_end_clean();
		}

		if ( is_numeric($pid) )
		{
			$this->ID = $pid;
		}

		// Start setting up the photos
		$this->videos = array();

		$repeater = get_field( 'videos', $this->ID );

		if ( $repeater )
		{

			foreach ( $repeater as $single )
			{
				$thumbnail = $this->get_youtube_image( $single['video'] );

				$this->videos[] = array(
					'url'       => $single['video'],
					'caption'   => $single['caption'],
					'thumbnail' => $thumbnail,
				);
			}
		}
	}
}
