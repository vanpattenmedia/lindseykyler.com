<?php


if (!class_exists('Timber')){
	echo 'Timber not activated';
}

$data  = Pagecraft::get_context();
$posts = Timber::get_posts('TimberPost');

$data['posts'] = $posts;

Timber::render('index.twig', $data);
