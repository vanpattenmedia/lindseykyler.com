<?php

add_theme_support('post-thumbnails');
add_theme_support('menus');

add_filter('get_twig', 'add_to_twig');

define('THEME_URL', get_template_directory_uri());

/**
 * Register custom nav menus
 */
register_nav_menus( array(
	'primary' => 'Primary Menu',
) );

/**
 * Pagecraft
 */
if ( class_exists('Timber') )
{
	class Pagecraft extends Timber
	{
		static function get_context()
		{
			$context = parent::get_context();

			$context['header'] = array();
			$context['header']['menu'] = new TimberMenu('primary');

			$context['pc'] = array();
			$context['pc']['contact'] = new PagecraftContact();
			$context['pc']['photos']  = new PagecraftPhotos();
			$context['pc']['resume']  = new PagecraftResume();
			$context['pc']['videos']  = new PagecraftVideos();

			return $context;
		}
	}
}

function add_to_twig($twig){
	/* this is where you can add your own fuctions to twig */
	$twig->addExtension(new Twig_Extension_StringLoader());
	return $twig;
}
