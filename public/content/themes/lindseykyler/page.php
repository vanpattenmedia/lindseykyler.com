<?php

$context = Pagecraft::get_context();

$post                  = new TimberPost();
$context['pc']['post'] = $post;

Timber::render(array('page-' . $post->post_name . '.twig', 'page.twig'), $context);
